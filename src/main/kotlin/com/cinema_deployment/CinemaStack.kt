package com.cinema_deployment

import com.cinema_deployment.Vpc.vpc
import com.cinema_deployment.backend.BackendCfnOutput
import com.cinema_deployment.backend.BackendEcs.backendEcs
import com.cinema_deployment.database.DatabaseCfnOutput.databaseCfnOutput
import com.cinema_deployment.database.DatabaseRds.databaseRds
import software.amazon.awscdk.Stack
import software.amazon.awscdk.StackProps
import software.constructs.Construct

class CinemaStack(scope: Construct, stackId: String, props: StackProps) : Stack(scope, stackId, props) {
    init {
        val vpc = vpc(this, stackId)

        val databaseRds = databaseRds(this, vpc)
        databaseCfnOutput(databaseRds, this)

        val backendEcs = backendEcs(databaseRds, this, vpc)
        BackendCfnOutput.backendCfnOutput(this, backendEcs)
    }
}
