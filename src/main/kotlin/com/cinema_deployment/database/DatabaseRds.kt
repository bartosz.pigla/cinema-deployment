package com.cinema_deployment.database

import com.cinema_deployment.CinemaStack
import com.cinema_deployment.database.DatabaseSecurityGroup.databaseSecurityGroup
import software.amazon.awscdk.Duration
import software.amazon.awscdk.RemovalPolicy
import software.amazon.awscdk.services.ec2.InstanceType
import software.amazon.awscdk.services.ec2.SubnetSelection
import software.amazon.awscdk.services.ec2.SubnetType
import software.amazon.awscdk.services.ec2.Vpc
import software.amazon.awscdk.services.rds.Credentials
import software.amazon.awscdk.services.rds.DatabaseInstance
import software.amazon.awscdk.services.rds.DatabaseInstanceEngine
import software.amazon.awscdk.services.rds.DatabaseInstanceProps

object DatabaseRds {
    const val databaseName = "cinemadb"
    const val databaseUsername = "postgres"

    fun databaseRds(cinemaStack: CinemaStack, vpc: Vpc): DatabaseInstance {
        val databaseRds = DatabaseInstance(
            cinemaStack,
            "cinemadb",
            DatabaseInstanceProps
                .builder()
                .vpc(vpc)
                .securityGroups(listOf(databaseSecurityGroup(cinemaStack, vpc)))
                .vpcSubnets(
                    SubnetSelection.builder()
                        .subnetType(SubnetType.PRIVATE_ISOLATED)
                        .build()
                )
                .engine(DatabaseInstanceEngine.POSTGRES)
                .instanceType(InstanceType("t3.micro"))
                .credentials(Credentials.fromGeneratedSecret(databaseUsername))
                .multiAz(false)
                .allocatedStorage(100)
                .allowMajorVersionUpgrade(true)
                .autoMinorVersionUpgrade(true)
                .backupRetention(Duration.days(0))
                .deleteAutomatedBackups(true)
                .removalPolicy(RemovalPolicy.DESTROY)
                .deletionProtection(false)
                .databaseName(databaseName)
                .publiclyAccessible(false)
                .build()
        )
        return databaseRds
    }
}
