package com.cinema_deployment.database

import com.cinema_deployment.CinemaStack
import software.amazon.awscdk.services.ec2.*

object DatabaseSecurityGroup {
    fun databaseSecurityGroup(cinemaStack: CinemaStack, vpc: Vpc): SecurityGroup {
        val databaseSecurityGroup = SecurityGroup(
            cinemaStack,
            "rds-sg",
            SecurityGroupProps.builder()
                .vpc(vpc)
                .build()
        )
        databaseSecurityGroup.addIngressRule(
            Peer.anyIpv4(),
            Port.tcp(5432),
            "allow db connections from anywhere"
        )
        return databaseSecurityGroup
    }
}
