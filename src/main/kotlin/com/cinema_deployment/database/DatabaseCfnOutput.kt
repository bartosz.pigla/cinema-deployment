package com.cinema_deployment.database

import com.cinema_deployment.CinemaStack
import software.amazon.awscdk.CfnOutput
import software.amazon.awscdk.CfnOutputProps
import software.amazon.awscdk.services.rds.DatabaseInstance

object DatabaseCfnOutput {
    fun databaseCfnOutput(databaseRds: DatabaseInstance, cinemaStack: CinemaStack) {
        CfnOutput(
            cinemaStack,
            "dbEndpoint",
            CfnOutputProps
                .builder()
                .value(databaseRds.instanceEndpoint.hostname)
                .build()
        )

        CfnOutput(
            cinemaStack,
            "secretName",
            CfnOutputProps
                .builder()
                .value(databaseRds.secret?.secretName)
                .build()
        )
    }
}
