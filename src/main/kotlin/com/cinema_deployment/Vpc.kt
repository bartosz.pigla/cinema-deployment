package com.cinema_deployment

import software.amazon.awscdk.services.ec2.SubnetConfiguration
import software.amazon.awscdk.services.ec2.SubnetType
import software.amazon.awscdk.services.ec2.Vpc
import software.amazon.awscdk.services.ec2.VpcProps

object Vpc {
    fun vpc(cinemaStack: CinemaStack, stackId: String): Vpc =
        Vpc(
            cinemaStack,
            stackId,
            VpcProps.builder()
                .cidr("10.0.0.0/16")
                .natGateways(0)
                .maxAzs(3)
                .subnetConfiguration(
                    listOf(
                        SubnetConfiguration.builder()
                            .name("public-subnet-1")
                            .subnetType(SubnetType.PUBLIC)
                            .cidrMask(24)
                            .build(),
                        SubnetConfiguration.builder()
                            .name("isolated-subnet-1")
                            .subnetType(SubnetType.PRIVATE_ISOLATED)
                            .cidrMask(28)
                            .build()
                    )
                )
                .build()
        )
}
