package com.cinema_deployment

import software.amazon.awscdk.App
import software.amazon.awscdk.Environment
import software.amazon.awscdk.StackProps
import java.lang.System.getenv

class CinemaApp {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val app = App()
            CinemaStack(
                app,
                "CinemaCdkStack-${getenv("BRANCH_NAME")}",
                StackProps.builder()
                    .env(
                        Environment.builder()
                            .account(getenv("AWS_ACCOUNT_ID"))
                            .region(getenv("AWS_DEFAULT_REGION"))
                            .build()
                    )
                    .build()
            )
            app.synth()
        }
    }
}
