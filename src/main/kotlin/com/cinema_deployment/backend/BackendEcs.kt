package com.cinema_deployment.backend

import com.cinema_deployment.database.DatabaseRds
import software.amazon.awscdk.Duration
import software.amazon.awscdk.services.ec2.Vpc
import software.amazon.awscdk.services.ecs.ContainerImage
import software.amazon.awscdk.services.ecs.Secret
import software.amazon.awscdk.services.ecs.patterns.ApplicationLoadBalancedFargateService
import software.amazon.awscdk.services.ecs.patterns.ApplicationLoadBalancedFargateService.Builder
import software.amazon.awscdk.services.ecs.patterns.ApplicationLoadBalancedTaskImageOptions
import software.amazon.awscdk.services.elasticloadbalancingv2.HealthCheck
import software.amazon.awscdk.services.rds.DatabaseInstance
import software.constructs.Construct
import java.lang.System.getenv

object BackendEcs {
    fun backendEcs(databaseRds: DatabaseInstance, scope: Construct, vpc: Vpc): ApplicationLoadBalancedFargateService {
        val ecs = Builder
            .create(scope, "cinema-backend")
            .vpc(vpc)
            .cpu(1024)
            .memoryLimitMiB(2048)
            .desiredCount(2)
            .taskImageOptions(
                ApplicationLoadBalancedTaskImageOptions
                    .builder()
                    .image(ContainerImage.fromRegistry(getenv("BACKEND_DOCKER_IMAGE")))
                    .containerPort(8080)
                    .secrets(
                        mapOf(
                            "SPRING_DATASOURCE_PASSWORD" to Secret.fromSecretsManager(databaseRds.secret!!, "password")
                        )
                    )
                    .environment(
                        mapOf(
                            "SPRING_DATASOURCE_URL" to "jdbc:postgresql://${databaseRds.dbInstanceEndpointAddress}:${databaseRds.dbInstanceEndpointPort}/${DatabaseRds.databaseName}",
                            "SPRING_DATASOURCE_USERNAME" to DatabaseRds.databaseUsername,
                            "OMDB_KEY" to "omdb-key",
                        )
                    )
                    .build()
            )
            .publicLoadBalancer(true)
            .assignPublicIp(true)
            .build()

        ecs.targetGroup.configureHealthCheck(
            HealthCheck
                .builder()
                .path("/actuator/health")
                .healthyHttpCodes("200")
                .port("8080")
                .interval(Duration.minutes(1))
                .unhealthyThresholdCount(3)
                .build()
        )
        return ecs
    }
}
