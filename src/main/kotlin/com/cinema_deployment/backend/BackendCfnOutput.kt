package com.cinema_deployment.backend

import com.cinema_deployment.CinemaStack
import software.amazon.awscdk.CfnOutput
import software.amazon.awscdk.CfnOutputProps
import software.amazon.awscdk.services.ecs.patterns.ApplicationLoadBalancedFargateService

object BackendCfnOutput {
    fun backendCfnOutput(cinemaStack: CinemaStack, backendEcs: ApplicationLoadBalancedFargateService) {
        CfnOutput(
            cinemaStack,
            "ecsPublicEndpoint",
            CfnOutputProps
                .builder()
                .value(backendEcs.loadBalancer.loadBalancerDnsName)
                .build()
        )
    }
}
