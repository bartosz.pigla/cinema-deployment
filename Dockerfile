FROM alpine:latest
RUN apk add --no-cache \
            python3 \
            py3-pip \
            nodejs \
            npm \
            openjdk11 \
        && pip3 install --upgrade pip \
        && pip3 install \
            awscli \
        && npm install -g aws-cdk \
        && rm -rf /var/cache/apk/* \
