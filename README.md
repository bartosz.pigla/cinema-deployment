# cinema-deployment

## How to deploy this?

`./gradlew shadowJar`

`cdk synth --output cdk.out --app 'java -jar build/libs/cinema-deployment-1.0-all.jar'`

`cdk deploy --app .\cdk.out\CinemaCdkStack.template.json`

## How to delete all remote AWS resources?

`cdk destroy --app .\cdk.out\CinemaCdkStack.template.json`

## Other commands

### Verify that given instance class can be used for given service

`aws rds describe-orderable-db-instance-options --engine postgres --db-instance-class t2.micro --region eu-central-1`
